<%-- 
    Document   : lista
    Created on : 24-oct-2016, 17:07:21
    Author     : usuario
--%>

<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Usuario enviado</h1>
        <p><%= request.getAttribute("nombre") %></p>
        <p><%= request.getAttribute("telefono") %></p>

        <h1>Usuarios en la sesión</h1>
        <%
        String nombre = "";

        Enumeration<String> nombres = session.getAttributeNames();
        out.println("<ul>");
        if (nombres.hasMoreElements()){
            while (nombres.hasMoreElements()) {
                nombre = nombres.nextElement();
                out.println("<li><b>" + nombre + ": </b>"
                        + session.getAttribute(nombre) + "</li>");
            }
        }else {
                out.println("<li><b>List vacía </b></li>");
        }
        out.println("</ul>");
        %>

        <% nombres = session.getAttributeNames();%>
        <ul>
        <%    
        if (nombres.hasMoreElements()){
            while (nombres.hasMoreElements()) {
                nombre = nombres.nextElement();
            %>
            <li><b> <%= nombre %> : </b>
                <%= session.getAttribute(nombre) %> 
            </li>
        
            <% }
        }else {
        %>
        <li><b>List vacía </b></li>
        <% } %>
        </ul>
    </body>
</html>
