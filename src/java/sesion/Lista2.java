/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alumno
 */
public class Lista2 extends HttpServlet {
    private String autor;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */



    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            ArrayList<String[]> lista = new ArrayList<>();      
            String nombre = "";
            String telefono = "";
            HttpSession sesion = request.getSession();
            Enumeration<String> atributos = sesion.getAttributeNames();
            while (atributos.hasMoreElements()){
                String atributo = atributos.nextElement();
                if (atributo.equals("lista")) {
                    lista = (ArrayList)sesion.getAttribute("lista");
//                    Array<String> nombres = sesion.getAttribute("nombres");
                    out.println("nueva llista<br>");
                }
            }
            
            
            Enumeration<String> parametros = request.getParameterNames();
            if ( parametros.hasMoreElements() ){
                nombre = request.getParameter("nombre");
                telefono = request.getParameter("telefono");
                String [] elemento = new String[2];
                elemento[0] = nombre;
                elemento[1] = telefono;
                lista.add(elemento);
                sesion.setAttribute("lista", lista);
            }
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Lista2</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Datos del formulario</h1>");
            out.println("<p>Nombre: " + nombre + "</p>");
            out.println("<p>Teléfono: " + telefono + "</p>");
            
            
            out.println("<h1>Lista usuarios en la sesión:</h1>");
            
            // Declaramos el Iterador e imprimimos los Elementos del ArrayList
            Iterator<String[]> iterator = lista.iterator();
            while(iterator.hasNext()){
                    String[] elemento = iterator.next();
                    out.println("<li>" + elemento[0] + ": " + elemento[1] + "</li>" );
            }          
            
            
            out.println("<hr>");
            out.println(this.autor + " - "  );
                      
            String organizacion = getServletConfig().getInitParameter("organizacion");
            out.println(organizacion);             
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
   @Override
    public void init(ServletConfig config){
         this.autor = config.getInitParameter("autor");
    }
}
